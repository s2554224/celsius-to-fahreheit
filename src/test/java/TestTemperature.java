import nl.utwente.di.CelsiusToFahrenheit.CelsiusToFahrenheit;
import nl.utwente.di.CelsiusToFahrenheit.Temperature;
import org.junit.*;

public class TestTemperature {

    @Test
    public void testTemperature1() throws Exception{
        Temperature temp = new Temperature();
        double fahrenheit = temp.convertTemperature("23");
        Assert.assertEquals("23 degrees celsius to fahrenheit", 73.4, fahrenheit, 0);
    }
}
