package nl.utwente.di.CelsiusToFahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class CelsiusToFahrenheit extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Temperature temp;

    public void init() throws ServletException{
        temp = new Temperature();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Celsius To Fahrenheit Converter Online";
        out.println(docType + "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius " +
                request.getParameter("Temperature") + "\n" +
                "  <P>Temperature in Fahrenheit is: " +
                Double.toString(temp.convertTemperature(request.getParameter("Temperature"))) +
                "</BODY></HTML>");
    }


}
