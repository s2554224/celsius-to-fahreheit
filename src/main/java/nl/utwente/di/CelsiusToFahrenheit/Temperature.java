package nl.utwente.di.CelsiusToFahrenheit;

public class Temperature {
    public double convertTemperature(String temperature) {
        return (Integer.parseInt(temperature) * 1.8) + 32;
    }
}
